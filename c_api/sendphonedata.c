#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include <errno.h>

int dynamic_bind_rc(int sock, struct sockaddr_rc *sockaddr, uint8_t *port)
{
    int err;
    for(*port = 1; *port <= 31; *port++) {
        sockaddr->rc_channel = *port;
        err = bind(sock, (struct sockaddr *)sockaddr, sizeof(sockaddr));
        if( ! err || errno == EINVAL ) break;
    }
    if(*port == 31) {
        err = -1;
        errno = EINVAL;
    }
    return err;
}

char buff[1024];

int main(int argc, char **argv) {
    struct sockaddr_rc addr = {0};
	uint8_t port;
	int s, status = 0;

//Alloc
//	char dest[18] = "BC:85:17:F3:33";
//Jessica
//	char dest[18] = "70:F9:27:8E:E2:1C";
//Grace
//	char dest[18] = "04:FE:31:3B:7D:F5";
//Caique
	char dest[18] = "04:FE:31:3B:7D:F5";

	s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

    addr.rc_family = AF_BLUETOOTH;
	addr.rc_channel = 16;
	//Possible way to get channel dynamically
	//dynamic_bind_rc(s, &addr, &port);
	
	str2ba(dest, &addr.rc_bdaddr);

	status = connect(s, (struct sockaddr *)&addr, sizeof(addr));
	
	printf("Conectado [%d]\n", status);

	while (1) {
		//read data function
		char current;
		int len = 0;
		while (1) {
			current = getchar();
			if (current == '\n') break;
			buff[len++] = current;
		}

		if (!strlen(buff)) break;

        status = write(s, buff, strlen(buff));

	    if (status < 0) {
			perror("uh oh");
		} else {
			fprintf(stderr, "Works\n");
		}
	}
    close(s);
    return 0;
}
