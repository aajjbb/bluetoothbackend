"""
    Class working with just one Android
"""
import sys
import bluetooth
import bluetooth.btcommon
import jsondata
import time
import os
import thread

uuid = "38709c11-9262-42a3-a0b1-be2ae5d52a90"

while True:
    service_matches = bluetooth.find_service(uuid = uuid)

    if len(service_matches) == 0:
        print ("couldn't find the FooBar service, trying once again")     

    for _ in service_matches:
    	port = _['port']
    	name = _['name']
    	host = _['host']
	
    	sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

        try:
            sock.connect((host, port))
        except bluetooth.btcommon.BluetoothError:
            print('error while connecting, try again later')
            continue
    

        print ("connecting to \"%s\" on %s - port: %s" % (name, host, port))

        os.system("sync; echo 3 | sudo tee /proc/sys/vm/drop_caches")
    
    	while True:
            raised = False
            time.sleep(2)
    
            print('Sending data to host: %s' % (str(host)))

            json_string = jsondata.build_json()

            try:
                sock.send(json_string)
            except bluetooth.btcommon.BluetoothError:
                print('Device disconnected')
                raised = True
                
            if raised:
                print('Raised')
                sock.close ()
                break
