"""
    Found BUG, trying multi-thread on server station.
"""
import sys
import bluetooth
import jsondata
import time
import os
import thread

#0 - grace, 1 - aloc 
uuids = ["38709c11-9262-42a3-a0b1-be2ae5d52a90", "38709c11-9262-42a3-a0b1-be2ae5d52a91"]

current = []
uuid_used = []

def run(host, port):
    sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    sock.connect((host, port))

    os.system("sync; echo 3 | sudo tee /proc/sys/vm/drop_caches")

    while True:
		print('Sending data to host: %s' % (str(host)))
		json_string = systeste.build_json()
		print(json_string)
		sock.send(json_string)

"""
Initial thread implementation. To be refactored
"""
while True:
	done = 0
	for i in uuids:
		if i in uuid_used:
			continue
	
		uuid_used.append(i)
		service_matches = bluetooth.find_service(uuid = i)

		for _ in service_matches:	
			port = _['port']
			name = _['name']
			host = _['host']

			if not host in current:
				current.append(host)
				thread.start_new_thread(run, (host, port))
				done = 1
				break
				
	        if len(service_matches) == 0:
				print ("couldn't find the FooBar service")     
#sender = ""
#while sender != "-1":
#	sender = str(raw_input())
#	if sender == "-1":
#		break
#	sock.send(sender)
#sock.close()

