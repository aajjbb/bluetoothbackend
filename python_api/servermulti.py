from __future__ import print_function

import threading
import bluetooth
import bluetooth.btcommon
import jsondata
import time 

active_host = []
idd = 1

class server (threading.Thread):
    def __init__(self, thread_id, host, port):
        super(server, self).__init__()

        self.thread_id = thread_id
        self.host = host
        self.port = port
        self.sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

        self._stop = threading.Event()

        try:
            self.sock.connect((host, port))
        except bluetooth.btcommon.BluetoothError:
            print("error connecting to host: %s in channel %s" % (str(host), str(port)))
            if self.host in active_host:
                active_host.remove(self.host)
            self.stop()

    def run(self):
        while not self._stop.isSet():
            self._stop.wait(2)
            raised = False
            
            print('Thread %d Sending data to host: %s though channel %s' % (int(self.thread_id), str(self.host), str(self.port)))

            json_string = jsondata.build_json()

            try:
                self.sock.send(json_string)
            except bluetooth.btcommon.BluetoothError:
                print('Device disconnected')
                raised = True
                
                if raised:
                    self.sock.close()
                    if self.host in active_host:
                        active_host.remove(self.host)
                    self.stop()

    def stop(self):
        self._stop.set()
        self._Thread__stop()

if __name__ == '__main__':
    uuids = ["38709c11-9262-42a3-a0b1-be2ae5d52a90", "38709c11-9262-42a3-a0b1-be2ae5d52a91", "38709c11-9262-42a3-a0b1-be2ae5d52a92"]

    while True:
        for uuid in uuids:
            service_matches = bluetooth.find_service(uuid = uuid)

            if len(service_matches) == 0:
                print ("couldn't find the FooBar service, trying once again")     
            else:
                for _ in service_matches:
                    port = _['port']
                    name = _['name']
                    host = _['host']
	
                    if not host in active_host:
                        print("Starting to send data to %s" % (host))
                        active_host.append(host)
                        now = server(idd, host, port)
                        now.start()
                        idd += 1
                    
