#!/usr/bin/env python
"""
    This class structure the data to send, in JSON format.
"""
import sysinfo
import sysinfotemperature
import json
import getpass
import commands
import os
import random
import sysinfotemperature

from collections import OrderedDict


"""
	Convert value from kb to gb
"""
def from_kb_to_gb(value):
    return float(float(value) / 1024.0 / 1024.0)

"""
	Convert value from b to gb
"""
def from_b_to_gb(value):
	return float(from_kb_to_gb(value) / 1024.0)

"""
	Build JSON string with current system info
"""
def build_json():
	"""
		system info layer 
	"""
	system_info = sysinfo.System().info()

	so = system_info[0]
	user = getpass.getuser()
	kernel_version = system_info[2]
	architecture = system_info[4]
	ip = commands.getoutput("/sbin/ifconfig").split("\n")[1].split()[1][5:]

	system_dict = OrderedDict(zip(['so', 'user', 'kernel_version', 'architecture', 'ip'], [so, user, kernel_version, architecture, ip]))

	"""
		hard drive info layer
	"""

	hdd = sysinfo.HDD();

	hdd_info = hdd.info()

	def hdd_usage(hdd_arg):
		free, total, used = 0, 0, 0

		for _ in hdd_arg:
			free += _['free']
			total += _['total']		
		
		used = total - free
		
		return (total, free, used)

	hdd_parsed_info = hdd_usage(hdd_info)

	total_hd_memory = from_b_to_gb(hdd_parsed_info[0])
	free_hd_memory = from_b_to_gb(hdd_parsed_info[1]) 
	used_hd_memory = from_b_to_gb(hdd_parsed_info[2])

	hdd_dict = OrderedDict(zip(['free', 'total', 'used'], [free_hd_memory, total_hd_memory, used_hd_memory]))

	"""
		ram info layer
	"""
	
	ram_info = sysinfo.RAM().info()

	total_ram_memory = from_kb_to_gb(ram_info['memtotal'])
	free_ram_memory = from_kb_to_gb(ram_info['memfree'])
	used_ram_memory = total_ram_memory - free_ram_memory

	ram_dict = OrderedDict(zip(['free', 'total', 'used'], [free_ram_memory, total_ram_memory, used_ram_memory]))
	
	"""
		cpu info layer
	"""

	cpu_info = sysinfo.CPU().info()

	cpu_cores, cpu_frequency, cores_model_name, cpu_cache_size = 0, 0, 0, 0

	for _ in cpu_info:
		if 'cpu cores' in _:
			cpu_cores += int(_['cpu cores'])
		if 'cpu mhz' in _:
			 cpu_frequency += float(_['cpu mhz'])			
		if 'model name' in _:
			cpu_model_name = _['model name']
		if 'cache size' in _:
			cpu_cache_size = _['cache size'][:-3]
        
	temperature = float(sysinfotemperature.getTemp(sysinfotemperature.hwcheck()))

	cpu_dict = OrderedDict(zip(['cores', 'core_frequency', 'temperature', 'model_name', 'cache_size'], [cpu_cores, cpu_frequency, temperature, cpu_model_name, cpu_cache_size]))

	previous_json = json.dumps({'data': OrderedDict(zip(['system', 'hdd', 'ram', 'cpu'], [system_dict, hdd_dict, ram_dict, cpu_dict]))})
	current_bcc = bcc(previous_json)

	final_json = json.dumps({'data': OrderedDict(zip(['system', 'hdd', 'ram', 'cpu', 'bcc_checksum'], [system_dict, hdd_dict, ram_dict, cpu_dict, current_bcc]))})
    
	return final_json

def bcc(string):
	ans = 0

	for i in string:
		ans = ans ^ int(ord(i))
	
	return ans

if __name__ == "__main__":
	pass
